package com.bip.schedule_app.value

object Updates {
    private var TEACHER: Boolean = false
    private var GROUP: Boolean = false
    private var POST: Boolean = false
    private var TIME: Boolean = false
    private var TEACHER_SCHEDULE: Boolean = false
    private var GROUP_SCHEDULE: Boolean = false

    var teacher: Boolean
    get() = TEACHER
    set(value) { TEACHER = value }

    var group: Boolean
    get() = GROUP
    set(value) { GROUP = value }

    var post: Boolean
    get() = POST
    set(value) { POST = value }

    var teacherSchedule: Boolean
    get() = TEACHER_SCHEDULE
    set(value) { TEACHER_SCHEDULE = value }

    var groupSchedule: Boolean
    get() = GROUP_SCHEDULE
    set(value) { GROUP_SCHEDULE = value }

    var time: Boolean
    get() = TIME
    set(value) { TIME = value }
}