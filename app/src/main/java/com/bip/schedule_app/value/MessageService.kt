package com.bip.schedule_app.value

import com.google.firebase.messaging.FirebaseMessaging

class MessageService {
    private fun subscribe(name: String) {
        FirebaseMessaging.getInstance().subscribeToTopic(name)
    }

    private fun unsubscribe(name: String) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(name)
    }

    fun subscribeToGroupMessaging(oldId: Int, newId: Int) {
        unsubscribe("group$oldId")
        subscribe("group$newId")
    }

    fun subscribeToTeacherMessaging(oldId: Int, newId: Int) {
        unsubscribe("teacher$oldId")
        subscribe("teacher$newId")
    }

    fun unsubscribeFromCurrentTeacher() {
        unsubscribe("teacher${UserSettings.teacher.id}")
    }

    fun subscribeToCurrentTeacher() {
        subscribe("teacher${UserSettings.teacher.id}")
    }

    fun unsubscribeFromCurrentGroup() {
        unsubscribe("group${UserSettings.group.id}")
    }

    fun subscribeToCurrentGroup() {
        subscribe("group${UserSettings.group.id}")
    }
}