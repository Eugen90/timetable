package com.bip.schedule_app.value

enum class Preferences {
    BiP,
    START_PAGE,
    SHOW_SECTION,
    NOTIFY,
    TEACHER_ID,
    TEACHER_NAME,
    GROUP_ID,
    GROUP_NAME,
    THEME
}