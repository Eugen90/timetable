package com.bip.schedule_app.value

import java.util.*

object States {
    var activeDay: Days
    var expandMode: Boolean = false

    init {
        activeDay = getWeekday()
    }

    private fun getWeekday(): Days {
        val calendar = Calendar.getInstance()
        return getActiveDayByNumber(calendar.get(Calendar.DAY_OF_WEEK))
    }

    fun getActiveDayByNumber(dayNumber: Int): Days {
        return when (dayNumber) {
            2 -> Days.MONDAY
            3 -> Days.TUESDAY
            4 -> Days.WEDNESDAY
            5 -> Days.THURSDAY
            6 -> Days.FRIDAY
            7 -> Days.SATURDAY
            else -> Days.MONDAY
        }
    }
}