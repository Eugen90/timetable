package com.bip.schedule_app.value
import android.os.Parcelable
import com.bip.schedule_app.R
import kotlinx.parcelize.Parcelize

@Parcelize
enum class Section(val value: Int, val page: Int): Parcelable {
    GROUP(0, R.id.groupScheduleFragment),
    TEACHER(1, R.id.teacherScheduleFragment),
    ALL(2, 0),
    NEVER(3, 0)
}