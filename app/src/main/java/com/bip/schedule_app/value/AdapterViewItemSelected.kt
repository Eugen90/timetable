package com.bip.schedule_app.value

import android.view.View
import android.widget.AdapterView

class AdapterViewItemSelected(private val callback: OnItemSelectCallback): AdapterView.OnItemSelectedListener {
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        callback.onItemSelected(parent, view, position, id)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}