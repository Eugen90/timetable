package com.bip.schedule_app.value

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import com.bip.schedule_app.model.Group
import com.bip.schedule_app.model.Teacher

object UserSettings {
    private var START_PAGE: Section = Section.GROUP
    private var SHOW_SECTION: Section = Section.ALL
    private var NOTIFICATION: Section = Section.ALL
    private var GROUP_ID: Int = 0
    private var GROUP_NAME: String = ""
    private var TEACHER_ID: Int = 0
    private var TEACHER_NAME: String = ""
    private var THEME: Theme = Theme.SYSTEM
    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(
            Preferences.BiP.toString(), AppCompatActivity.MODE_PRIVATE)
        START_PAGE = getSection(
            preferences.getString(Preferences.START_PAGE.toString(), Section.GROUP.name))
        SHOW_SECTION = getSection(
            preferences.getString(Preferences.SHOW_SECTION.toString(), Section.ALL.name))
        NOTIFICATION = getSection(
            preferences.getString(Preferences.NOTIFY.toString(), Section.ALL.name))
        TEACHER_ID = preferences.getInt(Preferences.TEACHER_ID.toString(), 0)
        TEACHER_NAME = preferences.getString(Preferences.TEACHER_NAME.toString(), "").toString()
        GROUP_ID = preferences.getInt(Preferences.GROUP_ID.toString(), 0)
        GROUP_NAME = preferences.getString(Preferences.GROUP_NAME.toString(), "").toString()
        THEME = getTheme(preferences.getString(Preferences.THEME.toString(), Theme.SYSTEM.name))
    }

    private fun getSection(name: String?): Section {
        return when(name) {
            Section.GROUP.name -> Section.GROUP
            Section.TEACHER.name -> Section.TEACHER
            Section.NEVER.name -> Section.NEVER
            else -> Section.ALL
        }
    }

    private fun getTheme(name: String?): Theme {
        return when(name) {
            Theme.LIGHT.name -> Theme.LIGHT
            Theme.DARK.name -> Theme.DARK
            else -> Theme.SYSTEM
        }
    }

    var teacher: Teacher
    get() = Teacher(TEACHER_ID, TEACHER_NAME)
    set(value) {
        TEACHER_NAME = value.name
        TEACHER_ID = value.id
        save(value.name, Preferences.TEACHER_NAME)
        save(value.id, Preferences.TEACHER_ID)
    }

    var group: Group
    get() = Group(GROUP_ID, GROUP_NAME)
    set(value) {
        GROUP_NAME = value.name
        GROUP_ID = value.id
        save(value.name, Preferences.GROUP_NAME)
        save(value.id, Preferences.GROUP_ID)
    }

    var startPage: Section
    get() = START_PAGE
    set(value) {
        START_PAGE = value
        save(value.name, Preferences.START_PAGE)
    }

    var section: Section
    get() = SHOW_SECTION
    set(value) {
        SHOW_SECTION = value
        save(value.name, Preferences.SHOW_SECTION)
    }

    var notify: Section
    get() = NOTIFICATION
    set(value) {
        NOTIFICATION = value
        save(value.name, Preferences.NOTIFY)
    }

    var theme: Theme
    get() = THEME
    set(value) {
        THEME = value
        save(value.name, Preferences.THEME)
    }

    private fun save(value: String, key: Preferences) {
        val editor = preferences.edit()
        editor.putString(key.toString(), value)
        editor.apply()
    }

    private fun save(value: Int, key: Preferences) {
        val editor = preferences.edit()
        editor.putInt(key.toString(), value)
        editor.apply()
    }
}