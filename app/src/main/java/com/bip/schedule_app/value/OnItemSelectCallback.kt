package com.bip.schedule_app.value

import android.view.View
import android.widget.AdapterView

interface OnItemSelectCallback {
    fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
}