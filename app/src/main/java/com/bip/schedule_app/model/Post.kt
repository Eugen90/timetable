package com.bip.schedule_app.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class Post(
    @PrimaryKey var id: Int,
    var title: String,
    var text: String,
    var image_url: String
): Parcelable
