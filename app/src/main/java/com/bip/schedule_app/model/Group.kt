package com.bip.schedule_app.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Group(
    @ColumnInfo(name = "group_id")
    @PrimaryKey var id: Int,
    @ColumnInfo(name = "group_name")
    @SerializedName("group_name") var name: String
)
