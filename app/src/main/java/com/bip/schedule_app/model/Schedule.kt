package com.bip.schedule_app.model

import com.bip.schedule_app.db.ScheduleDB
import com.bip.schedule_app.db.ScheduleType
import com.google.gson.annotations.SerializedName

data class Schedule(
    var number: Int,
    var group: Group,
    @SerializedName("week_day") var weekday: Weekday,
    @SerializedName("class") var lesson: Lesson,
    var teacher: Teacher,
    @SerializedName("classroom") var room: Room,
    @SerializedName("time_range") var time: Time
) {
    fun getScheduleDB(type: ScheduleType): ScheduleDB {
        return ScheduleDB(0, number, group, weekday, lesson, teacher, room, time, type.name)
    }
}
