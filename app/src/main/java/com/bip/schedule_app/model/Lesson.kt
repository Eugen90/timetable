package com.bip.schedule_app.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Lesson(
    @ColumnInfo(name = "lesson_id")
    val id: Int,
    @ColumnInfo(name = "lesson_name")
    @SerializedName("class_name") var name: String
)
