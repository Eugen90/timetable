package com.bip.schedule_app.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Weekday(
    @ColumnInfo(name = "weekday_id")
    val id: Int,
    @ColumnInfo(name = "day_name")
    @SerializedName("day_name") var name: String
)
