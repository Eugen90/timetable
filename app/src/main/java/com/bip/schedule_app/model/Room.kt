package com.bip.schedule_app.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Room(
    @ColumnInfo(name = "room_id")
    val id: Int,
    @ColumnInfo(name = "room_name")
    @SerializedName("classroom") var name: String
)
