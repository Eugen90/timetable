package com.bip.schedule_app.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Time(
    @ColumnInfo(name = "time_id")
    @PrimaryKey
    var id: Int,
    var range: String,
    @SerializedName("class_number") var lessonNumber: Int,
    @SerializedName("week_day_number") var dayNumber: Int
)
