package com.bip.schedule_app.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Teacher(
    @ColumnInfo(name = "teacher_id")
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "teacher_name")
    @SerializedName("fullName") val name: String
)
