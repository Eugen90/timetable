package com.bip.schedule_app.dialog

import android.app.Dialog
import android.view.Gravity
import android.widget.LinearLayout
import androidx.fragment.app.FragmentActivity
import com.bip.schedule_app.R
import com.bip.schedule_app.databinding.StandartMessageDialogBinding
import com.bip.schedule_app.extansion.postDelayed

fun FragmentActivity.showStandardMessage(message: String) {
    val binding = StandartMessageDialogBinding.inflate(layoutInflater)

    val dialog = Dialog(this, R.style.DialogTheme)
    dialog.setContentView(binding.root)
    dialog.window?.setLayout(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.WRAP_CONTENT
    )
    dialog.window?.setGravity(Gravity.BOTTOM)
    dialog.setCanceledOnTouchOutside(false)
    dialog.show()

    binding.message.text = message

    postDelayed({
        dialog.dismiss()
    }, 1800)
}