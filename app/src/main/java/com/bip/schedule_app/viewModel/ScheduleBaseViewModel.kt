package com.bip.schedule_app.viewModel

import androidx.lifecycle.LiveData
import com.bip.schedule_app.db.ScheduleDB
import com.bip.schedule_app.viewModel.BaseViewModel
import com.hadilq.liveevent.LiveEvent

abstract class ScheduleBaseViewModel: BaseViewModel() {
    abstract fun refreshSchedule()

    abstract val mondaySchedule: LiveData<MutableList<ScheduleDB>>
    abstract val tuesdaySchedule: LiveData<MutableList<ScheduleDB>>
    abstract val wednesdaySchedule: LiveData<MutableList<ScheduleDB>>
    abstract val thursdaySchedule: LiveData<MutableList<ScheduleDB>>
    abstract val fridaySchedule: LiveData<MutableList<ScheduleDB>>
    abstract val saturdaySchedule: LiveData<MutableList<ScheduleDB>>

    var mondayScheduleList: MutableList<ScheduleDB> = ArrayList()
    var tuesdayScheduleList: MutableList<ScheduleDB> = ArrayList()
    var wednesdayScheduleList: MutableList<ScheduleDB> = ArrayList()
    var thursdayScheduleList: MutableList<ScheduleDB> = ArrayList()
    var fridayScheduleList: MutableList<ScheduleDB> = ArrayList()
    var saturdayScheduleList: MutableList<ScheduleDB> = ArrayList()

    var refreshInLive = LiveEvent<Boolean>()
}