package com.bip.schedule_app.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bip.schedule_app.extansion.HandleError
import com.bip.schedule_app.extansion.InternalException
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel() {
    val errorLive = LiveEvent<HandleError>()

    fun doAsync(
        context: CoroutineContext = Dispatchers.IO,
        onError: ((Exception) -> Unit)? = null,
        request: suspend () -> Unit
    ) {
        viewModelScope.launch(context) {
            try {
                request()
            } catch (e: Exception) {
                val exception =
                    if (e !is InternalException) InternalException.UnknownException(e) else e
                onError?.invoke(exception)
                errorLive.postValue(HandleError(exception))
            }
        }
    }
}