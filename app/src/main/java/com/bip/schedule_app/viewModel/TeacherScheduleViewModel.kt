package com.bip.schedule_app.viewModel

import androidx.lifecycle.asLiveData
import com.bip.schedule_app.repos.TeacherScheduleRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TeacherScheduleViewModel @Inject constructor(
    private val repository: TeacherScheduleRepo
): ScheduleBaseViewModel() {

    override val mondaySchedule = repository.mondaySchedule.asLiveData()
    override val tuesdaySchedule = repository.tuesdaySchedule.asLiveData()
    override val wednesdaySchedule = repository.wednesdaySchedule.asLiveData()
    override val thursdaySchedule = repository.thursdaySchedule.asLiveData()
    override val fridaySchedule = repository.fridaySchedule.asLiveData()
    override val saturdaySchedule = repository.saturdaySchedule.asLiveData()

    override fun refreshSchedule() {
        doAsync {
            repository.refreshTeacherSchedule()
            refreshInLive.postValue(true)
        }
    }
}