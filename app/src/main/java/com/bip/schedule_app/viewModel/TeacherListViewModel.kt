package com.bip.schedule_app.viewModel

import androidx.lifecycle.*
import com.bip.schedule_app.MessagingService
import com.bip.schedule_app.repos.TeacherScheduleRepo
import com.bip.schedule_app.repos.TeachersRepo
import com.bip.schedule_app.viewModel.BaseViewModel
import com.hadilq.liveevent.LiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TeacherListViewModel @Inject constructor(
    private val repository: TeachersRepo,
    private val scheduleRepo: TeacherScheduleRepo
): BaseViewModel() {
    val teacherList = repository.teacherList.asLiveData()
    val saveInLive = LiveEvent<Boolean>()

    fun refreshTeacherList() {
        doAsync {
            repository.refreshTeacherList()
        }
    }

    fun updateTeacherSchedule() {
        doAsync {
            scheduleRepo.refreshTeacherSchedule()
            saveInLive.postValue(true)
        }
    }
}