package com.bip.schedule_app.viewModel

import androidx.lifecycle.asLiveData
import com.bip.schedule_app.repos.PostsRepo
import com.hadilq.liveevent.LiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PostsViewModel @Inject constructor(
    private val repository: PostsRepo
): BaseViewModel() {
    val postsList = repository.postsList.asLiveData()
    val refreshInLive = LiveEvent<Boolean>()

    fun refreshPostList() {
        doAsync {
            repository.refreshPosts()
            refreshInLive.postValue(true)
        }
    }
}