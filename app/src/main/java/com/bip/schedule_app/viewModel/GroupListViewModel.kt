package com.bip.schedule_app.viewModel

import androidx.lifecycle.asLiveData
import com.bip.schedule_app.repos.GroupScheduleRepo
import com.bip.schedule_app.repos.GroupsRepo
import com.bip.schedule_app.viewModel.BaseViewModel
import com.hadilq.liveevent.LiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class GroupListViewModel @Inject constructor(
    private val repo: GroupsRepo,
    private val scheduleRepo: GroupScheduleRepo
): BaseViewModel() {
    val groupList = repo.groupList.asLiveData()
    val saveInLive = LiveEvent<Boolean>()

    fun refreshGroupsList() {
        doAsync {
            repo.refreshGroupList()
        }
    }

    fun updateGroupSchedule() {
        doAsync {
            scheduleRepo.refreshSchedule()
            saveInLive.postValue(true)
        }
    }
}