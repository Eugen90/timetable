package com.bip.schedule_app

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.bip.schedule_app.value.Section
import com.bip.schedule_app.value.States
import com.bip.schedule_app.value.UserSettings
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MessagingService: FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage) {

        val body = message.data["body"] ?: ""
        val title = message.data["title"] ?: ""
        val weekday = message.data["weekDay"]
        val forWho = message.data["for"] ?: ""

        if (weekday != null) {
            States.activeDay = States.getActiveDayByNumber(weekday.toInt() + 1)
        }

        val section = getMessageSection(forWho)
        if (UserSettings.notify == Section.ALL || section == UserSettings.notify) {
            sendMessage(title, body, section.page)
        }
    }

    private fun getMessageSection(forWho: String): Section {
        return if (forWho == "teacher") Section.TEACHER
        else Section.GROUP
    }

    private fun sendMessage(title: String, message: String, destinationId: Int) {
        val options = BitmapFactory.Options()
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.message_icon, options)
        val channelId = createNotificationChannel(this)
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, channelId ?: "")
            .setLargeIcon(bitmap)
            .setSmallIcon(R.drawable.message_icon)
            .setAutoCancel(true)
            .setContentText(message)
            .setContentTitle(title)
            .setVibrate(longArrayOf(0, 500))
            .setStyle(NotificationCompat.BigTextStyle().bigText(message))
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(createPendingIntent(destinationId))

        val notificationManager =
            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, builder.build())
    }

    private fun createNotificationChannel(context: Context): String? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = getString(R.string.default_notification_channel_id)
            val channelName: CharSequence = "Bip"
            val channelDescription = "ScheduleApp"
            val channelImportance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(
                channelId,
                channelName, channelImportance
            )
            notificationChannel.description = channelDescription
            notificationChannel.enableVibration(true)
            val notificationManager =
                (context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
            notificationManager.createNotificationChannel(notificationChannel)
            channelId
        } else {
            null
        }
    }

    private fun createPendingIntent(destinationId: Int): PendingIntent {
        return NavDeepLinkBuilder(this)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.routes)
            .setDestination(destinationId)
            .createPendingIntent()
    }
}