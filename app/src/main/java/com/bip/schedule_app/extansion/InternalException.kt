package com.bip.schedule_app.extansion

import java.io.IOException
import java.lang.Exception

sealed class InternalException(override val message: String): IOException(message) {
    class ConnectionException : InternalException("No internet connection")
    class OperationException(message: String): InternalException(message)
    class UnknownException(val original: Exception): InternalException("Unknown error")
}

class HandleError(val exception: InternalException, val action: (() -> Unit)? = null)