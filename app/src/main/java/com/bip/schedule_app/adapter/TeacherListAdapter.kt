package com.bip.schedule_app.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bip.schedule_app.databinding.ListItemBinding
import com.bip.schedule_app.fragment.TeacherListFragment
import com.bip.schedule_app.model.Teacher

class TeacherListAdapter(private val teachers: MutableList<Teacher>,
                         private val itemClickListener: (Int) -> Unit):
    RecyclerView.Adapter<TeacherListAdapter.TeacherListViewHolder>() {

    class TeacherListViewHolder(val binding: ListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(teacher: Teacher, itemClickListener: (Int) -> Unit) {
            itemView.apply {
                binding.name.text = teacher.name
                binding.isChecked.visibility = if (teacher.id == TeacherListFragment.selectedTeacher.id) View.VISIBLE else View.INVISIBLE
            }
            itemView.setOnClickListener {
                TeacherListFragment.selectedTeacher = teacher
                itemClickListener(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeacherListViewHolder {
        val itemView = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TeacherListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TeacherListViewHolder, position: Int) {
        holder.bind(teachers[position], itemClickListener)
    }

    override fun getItemCount(): Int = teachers.size
}
