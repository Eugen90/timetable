package com.bip.schedule_app.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bip.schedule_app.databinding.PostItemBinding
import com.bumptech.glide.Glide
import com.bip.schedule_app.model.Post

class PostsAdapter(private val posts: MutableList<Post>,
                   private val itemClickListener: (View, Int) -> Unit):
    RecyclerView.Adapter<PostsAdapter.PostsViewHolder>(){

    class PostsViewHolder(val binding: PostItemBinding): BaseViewHolder(binding.root) {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun bind(post: Post, itemClickListener: (View, Int) -> Unit) {
            itemView.apply {
                Glide.with(binding.root).load(post.image_url).into(binding.postCardImage)
                binding.postCardTitle.text = post.title
                binding.postCardText.text = post.text
            }
            itemView.transitionName = post.id.toString()
            itemView.setOnClickListener { itemClickListener(itemView, adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val itemView = PostItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostsViewHolder(itemView)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.bind(posts[position], itemClickListener)
       // ViewAnimation(holder.lifecycleScope).fadeInTop(holder.binding.card, 35, 450)
    }

    override fun getItemCount(): Int = posts.size
}