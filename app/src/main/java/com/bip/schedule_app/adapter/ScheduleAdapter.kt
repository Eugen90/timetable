package com.bip.schedule_app.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bip.schedule_app.databinding.ScheduleItemBinding
import com.bip.schedule_app.db.ScheduleDB

class ScheduleAdapter(private val scheduleList: MutableList<ScheduleDB>, private val groupMode: Boolean):
    RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder>() {

    class ScheduleViewHolder(val binding: ScheduleItemBinding): BaseViewHolder(binding.root) {
        fun bind(schedule: ScheduleDB, groupMode: Boolean) {
            itemView.apply {
                binding.number.text = schedule.number.toString()
                binding.className.text = schedule.lesson.name
                binding.classRoom.text = schedule.room.name
                binding.timeRange.text = schedule.time.range
                binding.teacherName.text = if (groupMode) schedule.teacher.name else schedule.group.name
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        val itemView = ScheduleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ScheduleViewHolder(itemView)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
        holder.bind(scheduleList[position], groupMode)
        //ViewAnimation(holder.lifecycleScope).fadeInLeft(holder.binding.card, 48, 900)
    }

    override fun getItemCount(): Int = scheduleList.size


}