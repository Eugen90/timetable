package com.bip.schedule_app.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bip.schedule_app.databinding.ListItemBinding
import com.bip.schedule_app.fragment.GroupListFragment
import com.bip.schedule_app.model.Group

class GroupListAdapter(private val groups: MutableList<Group>,
                       private val itemClickListener: (Int) -> Unit):
    RecyclerView.Adapter<GroupListAdapter.GroupListViewHolder>(){

    class GroupListViewHolder(val binding: ListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(group: Group, itemClickListener: (Int) -> Unit) {
            itemView.apply {
                binding.name.text = group.name
                binding.isChecked.visibility = if (group.id == GroupListFragment.selectedGroup.id) View.VISIBLE else View.INVISIBLE
            }
            itemView.setOnClickListener {
                GroupListFragment.selectedGroup = group
                itemClickListener(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupListViewHolder {
        val itemView = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GroupListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: GroupListViewHolder, position: Int) {
        holder.bind(groups[position], itemClickListener)
    }

    override fun getItemCount(): Int = groups.size
}