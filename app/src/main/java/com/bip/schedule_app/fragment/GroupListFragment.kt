package com.bip.schedule_app.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bip.schedule_app.value.UserSettings
import com.bip.schedule_app.adapter.GroupListAdapter
import com.bip.schedule_app.databinding.FragmentGroupListBinding
import com.bip.schedule_app.model.Group
import com.bip.schedule_app.value.MessageService
import com.bip.schedule_app.value.Updates
import com.bip.schedule_app.viewModel.GroupListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class GroupListFragment : BaseFragment<FragmentGroupListBinding>() {
    companion object {
        var selectedGroup = Group(0, "")
        var selectedPosition = 0
    }
    private lateinit var adapter: GroupListAdapter
    private var groupList: MutableList<Group> = ArrayList()
    private val viewModel by viewModels<GroupListViewModel>()

    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentGroupListBinding =
        { inflater, parent, attach -> FragmentGroupListBinding.inflate(inflater, parent, attach) }

    override fun onInit() {
        super.onInit()
        if (!Updates.group) viewModel.refreshGroupsList()
        initGroupListView()
        selectedGroup = Group(UserSettings.group.id, UserSettings.group.name)
    }

    override fun onClickListeners() {
        super.onClickListeners()
        binding.back.setOnClickListener { navController.popBackStack() }
        binding.saveButton.setOnClickListener { save() }
    }

    override fun onViewLifecycle(owner: LifecycleOwner) {
        super.onViewLifecycle(owner)
        viewModel.groupList.observe(owner) {
            groupList.clear()
            groupList.addAll(it)
            selectedPosition = it.indexOfFirst { g -> g.id == selectedGroup.id }
            adapter.notifyItemRangeChanged(0, groupList.size)
        }
        viewModel.saveInLive.observe(viewLifecycleOwner) {
            if (it) navController.popBackStack()
        }
    }

    private fun save() {
        if (selectedGroup.id != UserSettings.group.id) {
            MessageService().subscribeToGroupMessaging(UserSettings.group.id, selectedGroup.id)
            UserSettings.group = selectedGroup
            viewModel.updateGroupSchedule()
        } else navController.popBackStack()
    }

    private fun initGroupListView() {
        adapter = GroupListAdapter(groupList, itemClick())
        binding.groupListView.layoutManager = LinearLayoutManager(context)
        binding.groupListView.adapter = adapter
    }

    private fun itemClick(): (Int) -> Unit {
        return {
            adapter.notifyItemChanged(it)
            adapter.notifyItemChanged(selectedPosition)
            selectedPosition = it
        }
    }
}