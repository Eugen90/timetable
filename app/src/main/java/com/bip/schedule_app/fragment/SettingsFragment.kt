package com.bip.schedule_app.fragment

import android.animation.LayoutTransition
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import com.bip.schedule_app.R
import com.bip.schedule_app.databinding.FragmentSettingsBinding
import com.bip.schedule_app.value.*

class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {
    private var backAddress: Section? = null

    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentSettingsBinding =
        {inflater, parent, attach -> FragmentSettingsBinding.inflate(inflater, parent, attach)}

    override fun onInit() {
        super.onInit()
        backAddress = arguments?.getParcelable("back")
        setStartPageListener()
        setThemeListener()
        setSectionSpinner()
        setNotificationSpinner()
        fillData()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        with(binding) {
            back.setOnClickListener { goBack() }
            teacherBox.setOnClickListener { navController.navigate(R.id.action_settingsFragment_to_teacherListFragment) }
            groupBox.setOnClickListener { navController.navigate(R.id.action_settingsFragment_to_groupListFragment) }
        }
    }

    private fun fillData() {
        with(binding) {
            if (UserSettings.group.name.isEmpty() && UserSettings.section != Section.TEACHER) {
                Button.text = getString(R.string.select_a_group)
                Button.setTextColor(Color.RED)
                groupPlaceholder.isVisible = false
            } else {
                groupPlaceholder.isVisible = true
                Button.text = UserSettings.group.name
            }
            if (UserSettings.teacher.name.isEmpty() && UserSettings.section != Section.GROUP) {
                teacherButton.text = getString(R.string.select_a_teacher)
                teacherButton.setTextColor(Color.RED)
                teacherPlaceholder.isVisible = false
            } else {
                teacherPlaceholder.isVisible = true
                teacherButton.text = UserSettings.teacher.name
            }
            when(UserSettings.theme) {
                Theme.LIGHT -> binding.lightTheme.isChecked = true
                Theme.DARK -> binding.darkTheme.isChecked = true
                else -> binding.systemTheme.isChecked = true
            }
        }
    }

    override var customBackNavigation = true
    override fun onCustomBackNavigation() {
        super.onCustomBackNavigation()
        goBack()
    }

    private fun goBack() {
        if (backAddress != null) {
            if (backAddress == Section.GROUP
                && UserSettings.section == Section.TEACHER) {
                    navController.navigate(R.id.action_settingsFragment_to_teacherScheduleFragment)
            }
            else if (backAddress == Section.TEACHER
                && UserSettings.section == Section.GROUP) {
                navController.navigate(R.id.action_settingsFragment_to_groupScheduleFragment)
            } else {
                navController.popBackStack()
            }
        } else {
            if (UserSettings.section == Section.TEACHER) {
                navController.navigate(R.id.action_settingsFragment_to_teacherScheduleFragment)
            } else {
                navController.navigate(R.id.action_settingsFragment_to_groupScheduleFragment)
            }
        }
    }

    private fun setStartPageListener() {
        binding.startPageGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.groupPage -> {
                    UserSettings.startPage = Section.GROUP
                }
                R.id.teacherPage -> {
                    UserSettings.startPage = Section.TEACHER
                }
            }
        }
    }

    private fun setThemeListener() {
        binding.themeGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.lightTheme -> {
                    setTheme(Theme.LIGHT)
                }
                R.id.darkTheme -> {
                    setTheme(Theme.DARK)
                }
                R.id.systemTheme -> {
                    setTheme(Theme.SYSTEM)
                }
            }
        }
    }

    private fun setSelectedStartPage() {
        binding.startPageGroup.clearCheck()
        binding.groupPage.isChecked = UserSettings.startPage == Section.GROUP
        binding.teacherPage.isChecked = UserSettings.startPage == Section.TEACHER
    }

    private fun setNotificationSpinner() {
        setNotificationListener()
        ArrayAdapter.createFromResource(requireContext(), R.array.notifications,
            android.R.layout.simple_spinner_item).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.notificationsSpinner.adapter = adapter
        }
        binding.notificationsSpinner.setSelection(UserSettings.notify.value)
    }

    private fun setSectionSpinner() {
        setSectionListener()
        ArrayAdapter.createFromResource(requireContext(), R.array.sections,
            android.R.layout.simple_spinner_item).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.sectionsSpinner.adapter = adapter
        }
        binding.sectionsSpinner.setSelection(UserSettings.section.value)
    }

    private fun setNotificationListener() {
        val listener = AdapterViewItemSelected(object : OnItemSelectCallback {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position) {
                    0 -> UserSettings.notify = Section.GROUP
                    1 -> UserSettings.notify = Section.TEACHER
                    2 -> UserSettings.notify = Section.ALL
                    3 -> {
                        UserSettings.notify = Section.NEVER
                        MessageService().unsubscribeFromCurrentTeacher()
                        MessageService().unsubscribeFromCurrentGroup()
                    }
                }
            }
        })
        binding.notificationsSpinner.onItemSelectedListener = listener
    }

    private fun setSectionListener() {
        val listener = AdapterViewItemSelected(object : OnItemSelectCallback {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    Section.GROUP.value -> { setGroupSection() }
                    Section.TEACHER.value -> { setTeacherSection() }
                    Section.ALL.value -> { setAllSection() }
                }
            }
        })
        binding.sectionsSpinner.onItemSelectedListener = listener
    }

    private fun setGroupSection() {
        hideStartPageBox()
        UserSettings.section = Section.GROUP
        UserSettings.startPage = Section.GROUP
        if (UserSettings.notify != Section.NEVER) {
            MessageService().unsubscribeFromCurrentTeacher()
            MessageService().subscribeToCurrentGroup()
            UserSettings.notify = Section.GROUP
            binding.notificationsSpinner.setSelection(Section.GROUP.value)
        }
    }

    private fun setTeacherSection() {
        hideStartPageBox()
        UserSettings.section = Section.TEACHER
        UserSettings.startPage = Section.TEACHER
        if (UserSettings.notify != Section.NEVER) {
            MessageService().unsubscribeFromCurrentGroup()
            MessageService().subscribeToCurrentTeacher()
            UserSettings.notify = Section.TEACHER
            binding.notificationsSpinner.setSelection(Section.TEACHER.value)
        }
    }

    private fun setAllSection() {
        showStartPageBox()
        UserSettings.section = Section.ALL
    }

    private fun hideStartPageBox() {
        (binding.startPageBox as ViewGroup).layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        binding.startPageBox.updateLayoutParams { height = 0 }
    }

    private fun showStartPageBox() {
        (binding.startPageBox as ViewGroup).layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        binding.startPageBox.updateLayoutParams { height = binding.groupAndTeacherBox.height }
        setSelectedStartPage()
    }
}