package com.bip.schedule_app.fragment

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import com.bip.schedule_app.R
import com.bip.schedule_app.value.States
import com.bip.schedule_app.adapter.ScheduleAdapter
import com.bip.schedule_app.db.ScheduleDB
import com.bip.schedule_app.value.Days
import com.bip.schedule_app.viewModel.ScheduleBaseViewModel

abstract class ScheduleBaseFragment<VB: ViewBinding, VM: ScheduleBaseViewModel>: BaseFragment<VB>() {
    protected lateinit var viewModel: VM
    protected abstract val viewModelClass: Class<VM>
    protected var mondaySchedule: MutableList<ScheduleDB> = ArrayList()
    protected var tuesdaySchedule: MutableList<ScheduleDB> = ArrayList()
    protected var wednesdaySchedule: MutableList<ScheduleDB> = ArrayList()
    protected var thursdaySchedule: MutableList<ScheduleDB> = ArrayList()
    protected var fridaySchedule: MutableList<ScheduleDB> = ArrayList()
    protected var saturdaySchedule: MutableList<ScheduleDB> = ArrayList()

    protected lateinit var mondayAdapter: ScheduleAdapter
    protected lateinit var tuesdayAdapter: ScheduleAdapter
    protected lateinit var wednesdayAdapter: ScheduleAdapter
    protected lateinit var thursdayAdapter: ScheduleAdapter
    protected lateinit var fridayAdapter: ScheduleAdapter
    protected lateinit var saturdayAdapter: ScheduleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(viewModelClass)
    }

    fun setOnRefreshListener(view: SwipeRefreshLayout) {
        setSwipeRefreshColors(view)
        view.setOnRefreshListener {
            viewModel.refreshSchedule()
        }
    }

    private fun setSwipeRefreshColors(view: SwipeRefreshLayout) {
        val orange = ContextCompat.getColor(requireContext(), R.color.orange)
        val blue = ContextCompat.getColor(requireContext(), R.color.blue)
        view.setColorSchemeColors(orange, blue)
    }

    override fun onViewLifecycle(owner: LifecycleOwner) {
        super.onViewLifecycle(owner)
        viewModel.mondaySchedule.observe(owner) {
            viewModel.mondayScheduleList.clear()
            viewModel.mondayScheduleList.addAll(it)
            if (States.activeDay == Days.MONDAY) getTodaySchedule()
        }
        viewModel.tuesdaySchedule.observe(owner) {
            viewModel.tuesdayScheduleList.clear()
            viewModel.tuesdayScheduleList.addAll(it)
            if (States.activeDay == Days.TUESDAY) getTodaySchedule()
        }
        viewModel.wednesdaySchedule.observe(owner) {
            viewModel.wednesdayScheduleList.clear()
            viewModel.wednesdayScheduleList.addAll(it)
            if (States.activeDay == Days.WEDNESDAY) getTodaySchedule()
        }
        viewModel.thursdaySchedule.observe(owner) {
            viewModel.thursdayScheduleList.clear()
            viewModel.thursdayScheduleList.addAll(it)
            if (States.activeDay == Days.THURSDAY) getTodaySchedule()
        }
        viewModel.fridaySchedule.observe(owner) {
            viewModel.fridayScheduleList.clear()
            viewModel.fridayScheduleList.addAll(it)
            if (States.activeDay == Days.FRIDAY) getTodaySchedule()
        }
        viewModel.saturdaySchedule.observe(owner) {
            viewModel.saturdayScheduleList.clear()
            viewModel.saturdayScheduleList.addAll(it)
            if (States.activeDay == Days.SATURDAY) getTodaySchedule()
        }
    }

    private fun getMondaySchedule() {
        val schedule = viewModel.mondayScheduleList
        if (mondaySchedule.isNotEmpty()) mondaySchedule.clear()
        mondaySchedule.addAll(schedule)
        mondayAdapter.notifyItemRangeChanged(0, schedule.size)
    }

    private fun getTuesdaySchedule() {
        val schedule = viewModel.tuesdayScheduleList
        if (tuesdaySchedule.isNotEmpty()) tuesdaySchedule.clear()
        tuesdaySchedule.addAll(schedule)
        tuesdayAdapter.notifyItemRangeChanged(0, schedule.size)
    }

    private fun getWednesdaySchedule() {
        val schedule = viewModel.wednesdayScheduleList
        if (wednesdaySchedule.isNotEmpty()) wednesdaySchedule.clear()
        wednesdaySchedule.addAll(schedule)
        wednesdayAdapter.notifyItemRangeChanged(0, schedule.size)
    }

    private fun getThursdaySchedule() {
        val schedule = viewModel.thursdayScheduleList
        if (thursdaySchedule.isNotEmpty()) thursdaySchedule.clear()
        thursdaySchedule.addAll(schedule)
        thursdayAdapter.notifyItemRangeChanged(0, schedule.size)
    }

    private fun getFridaySchedule() {
        val schedule = viewModel.fridayScheduleList
        if (fridaySchedule.isNotEmpty()) fridaySchedule.clear()
        fridaySchedule.addAll(schedule)
        fridayAdapter.notifyItemRangeChanged(0, schedule.size)
    }

    private fun getSaturdaySchedule() {
        val schedule = viewModel.saturdayScheduleList
        if (saturdaySchedule.isNotEmpty()) saturdaySchedule.clear()
        saturdaySchedule.addAll(schedule)
        saturdayAdapter.notifyItemRangeChanged(0, schedule.size)
    }

    open fun clearAll() {
        mondayAdapter.notifyItemRangeChanged(0, mondaySchedule.size)
        mondaySchedule.clear()
        tuesdayAdapter.notifyItemRangeChanged(0, tuesdaySchedule.size)
        tuesdaySchedule.clear()
        wednesdayAdapter.notifyItemRangeChanged(0, wednesdaySchedule.size)
        wednesdaySchedule.clear()
        thursdayAdapter.notifyItemRangeChanged(0, thursdaySchedule.size)
        thursdaySchedule.clear()
        fridayAdapter.notifyItemRangeChanged(0, fridaySchedule.size)
        fridaySchedule.clear()
        saturdayAdapter.notifyItemRangeChanged(0, saturdaySchedule.size)
        saturdaySchedule.clear()

    }

    open fun expand() {
        States.expandMode = true
        getMondaySchedule()
        getTuesdaySchedule()
        getWednesdaySchedule()
        getThursdaySchedule()
        getFridaySchedule()
        getSaturdaySchedule()
    }

    open fun collapse() {
        States.expandMode = false
        clearAll()
        activateDay(States.activeDay)
    }

    open fun activateDay(day: Days) {
        States.activeDay = day
        if (States.expandMode) {
            collapse()
        } else {
            clearAll()
            when (day) {
                Days.MONDAY -> getMondaySchedule()
                Days.TUESDAY -> getTuesdaySchedule()
                Days.WEDNESDAY -> getWednesdaySchedule()
                Days.THURSDAY -> getThursdaySchedule()
                Days.FRIDAY -> getFridaySchedule()
                Days.SATURDAY -> getSaturdaySchedule()
            }
        }
    }

    open fun getTodaySchedule() {
        if (!States.expandMode) {
            when (States.activeDay) {
                Days.MONDAY -> getMondaySchedule()
                Days.TUESDAY -> getTuesdaySchedule()
                Days.WEDNESDAY -> getWednesdaySchedule()
                Days.THURSDAY -> getThursdaySchedule()
                Days.FRIDAY -> getFridaySchedule()
                Days.SATURDAY -> getSaturdaySchedule()
            }
        } else {
            expand()
        }
    }
}