package com.bip.schedule_app.fragment

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.navigation.fragment.navArgs
import com.bip.schedule_app.R
import com.bip.schedule_app.databinding.FragmentPostDetailsBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bip.schedule_app.model.Post
import com.google.android.material.transition.MaterialContainerTransform

class PostDetailsFragment : BaseFragment<FragmentPostDetailsBinding>() {
    private lateinit var post: Post
    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentPostDetailsBinding =
        { inflater, parent, attach -> FragmentPostDetailsBinding.inflate(inflater, parent, attach) }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = MaterialContainerTransform().apply {
            drawingViewId = R.id.nav_host_fragment
            duration = 800
            scrimColor = Color.TRANSPARENT
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val args: PostDetailsFragmentArgs by navArgs()
        post = args.postArgs
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onInit() {
        super.onInit()
        Glide.with(this).load(post.image_url).into(binding.postDetailsImage)
            .apply { RequestOptions.noTransformation().onlyRetrieveFromCache(true) }
        with(binding) {
            postDetailsTitle.text = post.title
            postDetailsText.text = post.text
            back.setOnClickListener { navController.popBackStack() }

        }
    }
}