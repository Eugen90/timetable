package com.bip.schedule_app.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bip.schedule_app.value.UserSettings
import com.bip.schedule_app.adapter.TeacherListAdapter
import com.bip.schedule_app.databinding.FragmentTeacherListBinding
import com.bip.schedule_app.model.Teacher
import com.bip.schedule_app.value.MessageService
import com.bip.schedule_app.value.Updates
import com.bip.schedule_app.viewModel.TeacherListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class TeacherListFragment : BaseFragment<FragmentTeacherListBinding>() {
    companion object {
        var selectedTeacher = Teacher(0, "")
        var selectedPosition = 0
    }
    private lateinit var adapter: TeacherListAdapter
    private var teacherList: MutableList<Teacher> = ArrayList()
    private val viewModel by viewModels<TeacherListViewModel>()

    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentTeacherListBinding =
        { inflater, parent, attach -> FragmentTeacherListBinding.inflate(inflater, parent, attach) }

    override fun onInit() {
        super.onInit()
        if (!Updates.teacher) viewModel.refreshTeacherList()
        initTeacherListView()
        selectedTeacher = Teacher(UserSettings.teacher.id, UserSettings.teacher.name)
    }

    override fun onClickListeners() {
        super.onClickListeners()
        binding.back.setOnClickListener{ navController.popBackStack() }
        binding.saveButton.setOnClickListener { save() }
    }

    override fun onViewLifecycle(owner: LifecycleOwner) {
        super.onViewLifecycle(owner)
        viewModel.teacherList.observe(owner) {
            teacherList.clear()
            teacherList.addAll(it)
            selectedPosition = it.indexOfFirst { t -> t.id == selectedTeacher.id }
            adapter.notifyItemRangeChanged(0, it.size)
        }
        viewModel.saveInLive.observe(viewLifecycleOwner) {
            if (it) navController.popBackStack()
        }
    }

    private fun save() {
        if (selectedTeacher.id != UserSettings.teacher.id) {
            MessageService().subscribeToTeacherMessaging(UserSettings.teacher.id, selectedTeacher.id)
            UserSettings.teacher = selectedTeacher
            viewModel.updateTeacherSchedule()
        } else navController.popBackStack()
    }

    private fun initTeacherListView() {
        adapter = TeacherListAdapter(teacherList, itemClick())
        binding.teacherListView.layoutManager = LinearLayoutManager(context)
        binding.teacherListView.adapter = adapter
    }

    private fun itemClick(): (Int) -> Unit {
        return {
            adapter.notifyItemChanged(it)
            adapter.notifyItemChanged(selectedPosition)
            selectedPosition = it
        }
    }
}