package com.bip.schedule_app.fragment

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.bip.schedule_app.R
import com.bip.schedule_app.value.UserSettings
import com.bip.schedule_app.databinding.FragmentAboutBinding
import com.bip.schedule_app.value.Section
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AboutFragment : BaseFragment<FragmentAboutBinding>() {
    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentAboutBinding =
        { inflater, parent, attach -> FragmentAboutBinding.inflate(inflater, parent, attach) }

    override fun onInit() {
        super.onInit()
        if (UserSettings.section == Section.TEACHER) hideGroupMenu()
        if (UserSettings.section == Section.GROUP) hideTeacherMenu()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        with(binding) {
            setting.setOnClickListener { navController.navigate(R.id.action_aboutFragment_to_settingsFragment) }
            postsButton.setOnClickListener { navController.navigate(R.id.action_aboutFragment_to_postsFragment) }
            groupButton.setOnClickListener { navController.navigate(R.id.action_aboutFragment_to_groupScheduleFragment) }
            teacherButton.setOnClickListener { navController.navigate(R.id.action_aboutFragment_to_teacherScheduleFragment) }
            more.setOnClickListener { goToAddress(getString(R.string.scool_info)) }
            site.setOnClickListener { goToAddress(getString(R.string.our_site)) }
        }
    }

    private fun hideGroupMenu() {
        binding.groupButton.visibility = View.INVISIBLE
        val params = binding.teacherButton.layoutParams as ConstraintLayout.LayoutParams
        params.horizontalBias = 0.5f
        binding.teacherButton.layoutParams = params
    }

    private fun hideTeacherMenu() {
        binding.teacherButton.visibility = View.INVISIBLE
        val params = binding.groupButton.layoutParams as ConstraintLayout.LayoutParams
        params.horizontalBias = 0.5f
        binding.groupButton.layoutParams = params
    }

    private fun goToAddress(address: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(address))
        startActivity(browserIntent)
    }
}