package com.bip.schedule_app.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import com.bip.schedule_app.R
import com.bip.schedule_app.value.UserSettings
import com.bip.schedule_app.adapter.ScheduleAdapter
import com.bip.schedule_app.databinding.FragmentTeacherScheduleBinding
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.value.Days
import com.bip.schedule_app.value.Section
import com.bip.schedule_app.value.Updates
import com.bip.schedule_app.viewModel.TeacherScheduleViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TeacherScheduleFragment : ScheduleBaseFragment<FragmentTeacherScheduleBinding, TeacherScheduleViewModel>() {
    override val viewModelClass: Class<TeacherScheduleViewModel> = TeacherScheduleViewModel::class.java
    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentTeacherScheduleBinding =
        { inflater, parent, attach -> FragmentTeacherScheduleBinding.inflate(inflater, parent, attach) }

    override fun onInit() {
        super.onInit()
        if (!Updates.teacherSchedule) viewModel.refreshSchedule()
        if (UserSettings.section == Section.TEACHER) hideGroupMenu()
        binding.teacherName.text = UserSettings.teacher.name
        setOnRefreshListener(binding.refreshSchedule)
        initMondayView()
        initTuesdayView()
        initWednesdayView()
        initThursdayView()
        initFridayView()
        initSaturdayView()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        with(binding) {
            settingButton.setOnClickListener { navController.navigate(
                R.id.action_teacherScheduleFragment_to_settingsFragment,
                bundleOf("back" to Section.TEACHER)) }
            posts.setOnClickListener { navController.navigate(
                R.id.action_teacherScheduleFragment_to_postsFragment) }
            groupMenu.setOnClickListener { navController.navigate(
                R.id.action_teacherScheduleFragment_to_groupScheduleFragment) }
            aboutMenu.setOnClickListener { navController.navigate(
                R.id.action_teacherScheduleFragment_to_aboutFragment) }
            expandButton.setOnClickListener { expand() }
            collapseButton.setOnClickListener { collapse() }
            monday.setOnClickListener { activateDay(Days.MONDAY) }
            tuesday.setOnClickListener { activateDay(Days.TUESDAY) }
            wednesday.setOnClickListener { activateDay(Days.WEDNESDAY) }
            thursday.setOnClickListener { activateDay(Days.THURSDAY) }
            friday.setOnClickListener { activateDay(Days.FRIDAY) }
            saturday.setOnClickListener { activateDay(Days.SATURDAY) }
        }
    }

    override fun onViewLifecycle(owner: LifecycleOwner) {
        super.onViewLifecycle(owner)
        viewModel.refreshInLive.observe(owner) {
            binding.refreshSchedule.isRefreshing = false
        }
        viewModel.errorLive.observe(owner) {
            binding.refreshSchedule.isRefreshing = false
            if (it.exception is InternalException.OperationException) {
                showMessage(it.exception.message)
            } else showMessage(getString(R.string.unknown_exception))
        }
    }

    private fun hideGroupMenu() {
        binding.groupMenu.visibility = View.INVISIBLE
        val params = binding.teacherButton.layoutParams as ConstraintLayout.LayoutParams
        params.horizontalBias = 0.5f
        binding.teacherButton.layoutParams = params
    }

    override fun expand() {
        super.expand()
        binding.expandButton.visibility = View.INVISIBLE
        binding.collapseButton.visibility = View.VISIBLE
        binding.menuBox.visibility = View.INVISIBLE
    }

    override fun collapse() {
        super.collapse()
        binding.expandButton.visibility = View.VISIBLE
        binding.collapseButton.visibility = View.INVISIBLE
        binding.menuBox.visibility = View.VISIBLE
    }

    private fun initMondayView() {
        mondayAdapter = ScheduleAdapter(mondaySchedule, false)
        binding.mondayView.layoutManager = LinearLayoutManager(context)
        binding.mondayView.adapter = mondayAdapter
    }

    private fun initTuesdayView() {
        tuesdayAdapter = ScheduleAdapter(tuesdaySchedule, false)
        binding.tuesdayView.layoutManager = LinearLayoutManager(context)
        binding.tuesdayView.adapter = tuesdayAdapter
    }

    private fun initWednesdayView() {
        wednesdayAdapter = ScheduleAdapter(wednesdaySchedule, false)
        binding.wednesdayView.layoutManager = LinearLayoutManager(context)
        binding.wednesdayView.adapter = wednesdayAdapter
    }

    private fun initThursdayView() {
        thursdayAdapter = ScheduleAdapter(thursdaySchedule, false)
        binding.thursdayView.layoutManager = LinearLayoutManager(context)
        binding.thursdayView.adapter = thursdayAdapter
    }

    private fun initFridayView() {
        fridayAdapter = ScheduleAdapter(fridaySchedule, false)
        binding.fridayView.layoutManager = LinearLayoutManager(context)
        binding.fridayView.adapter = fridayAdapter
    }

    private fun initSaturdayView() {
        saturdayAdapter = ScheduleAdapter(saturdaySchedule, false)
        binding.saturdayView.layoutManager = LinearLayoutManager(context)
        binding.saturdayView.adapter = saturdayAdapter
    }
}