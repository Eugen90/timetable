package com.bip.schedule_app.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.LinearLayoutManager
import com.bip.schedule_app.R
import com.bip.schedule_app.adapter.PostsAdapter
import com.bip.schedule_app.databinding.FragmentPostsBinding
import com.bip.schedule_app.model.Post
import com.bip.schedule_app.value.Section
import com.bip.schedule_app.value.Updates
import com.bip.schedule_app.value.UserSettings
import com.bip.schedule_app.viewModel.PostsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostsFragment : BaseFragment<FragmentPostsBinding>() {
    private lateinit var adapter: PostsAdapter
    private var postsList: MutableList<Post> = ArrayList()
    private val viewModel by viewModels<PostsViewModel>()

    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentPostsBinding =
        { inflater, parent, attach -> FragmentPostsBinding.inflate(inflater, parent, attach) }

    override fun onInit() {
        super.onInit()
        if (!Updates.post) viewModel.refreshPostList()
        postponeEnterTransition()
        binding.root.doOnPreDraw { startPostponedEnterTransition() }
        if (UserSettings.section == Section.TEACHER) hideGroupMenu()
        if (UserSettings.section == Section.GROUP) hideTeacherMenu()
        initPostsView()
        binding.postsRefresh.setOnRefreshListener { viewModel.refreshPostList() }
    }

    override fun onClickListeners() {
        super.onClickListeners()
        with(binding) {
            setting.setOnClickListener { navController.navigate(R.id.action_postsFragment_to_settingsFragment) }
            group.setOnClickListener { navController.navigate(R.id.action_postsFragment_to_groupScheduleFragment) }
            teacher.setOnClickListener { navController.navigate(R.id.action_postsFragment_to_teacherScheduleFragment) }
            about.setOnClickListener { navController.navigate(R.id.action_postsFragment_to_aboutFragment) }
        }
    }

    override fun onViewLifecycle(owner: LifecycleOwner) {
        super.onViewLifecycle(owner)
        viewModel.postsList.observe(viewLifecycleOwner) {
            postsList.clear()
            postsList.addAll(it)
            adapter.notifyItemRangeChanged(0, it.size)
        }
        viewModel.refreshInLive.observe(owner) {
            binding.postsRefresh.isRefreshing = false
        }
    }

    private fun initPostsView() {
        adapter = PostsAdapter(postsList, itemClick())
        binding.postsView.layoutManager = LinearLayoutManager(context)
        binding.postsView.adapter = adapter
    }

    private fun itemClick(): (View, Int) -> Unit {
        return { view, position ->
            val extras = FragmentNavigatorExtras(
                view to getString(R.string.post_transation_name)
            )
            val action =
                PostsFragmentDirections
                    .actionPostsFragmentToPostDetailsFragment(postsList[position])
            navController.navigate(action, extras)
        }
    }

    private fun hideGroupMenu() {
        binding.group.visibility = View.INVISIBLE
        val params = binding.teacher.layoutParams as ConstraintLayout.LayoutParams
        params.horizontalBias = 0.5f
        binding.teacher.layoutParams = params
    }

    private fun hideTeacherMenu() {
        binding.teacher.visibility = View.INVISIBLE
        val params = binding.group.layoutParams as ConstraintLayout.LayoutParams
        params.horizontalBias = 0.5f
        binding.group.layoutParams = params
    }
}