package com.bip.schedule_app.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import com.bip.schedule_app.R
import com.bip.schedule_app.databinding.FragmentStartBinding
import com.bip.schedule_app.value.Section
import com.bip.schedule_app.value.Theme
import com.bip.schedule_app.value.UserSettings

class StartFragment : BaseFragment<FragmentStartBinding>() {
    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentStartBinding =
        { inflater, parent, attach -> FragmentStartBinding.inflate(inflater, parent, attach) }

    override fun onInit() {
        UserSettings.init(requireContext())

        setTheme(UserSettings.theme)

        if (UserSettings.group.id == 0 && UserSettings.teacher.id == 0) {
            navController.navigate(R.id.action_startFragment_to_settingsFragment)
        } else {
            if (UserSettings.startPage == Section.GROUP) {
                navController.navigate(R.id.action_startFragment_to_groupScheduleFragment)
            } else if (UserSettings.startPage == Section.TEACHER) {
                navController.navigate(R.id.action_startFragment_to_teacherScheduleFragment)
            }
        }
    }
}