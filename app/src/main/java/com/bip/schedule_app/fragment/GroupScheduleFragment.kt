package com.bip.schedule_app.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import com.bip.schedule_app.R
import com.bip.schedule_app.value.UserSettings
import com.bip.schedule_app.adapter.ScheduleAdapter
import com.bip.schedule_app.databinding.FragmentGroupScheduleBinding
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.value.Days
import com.bip.schedule_app.value.Section
import com.bip.schedule_app.value.Updates
import com.bip.schedule_app.viewModel.GroupScheduleViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GroupScheduleFragment : ScheduleBaseFragment<FragmentGroupScheduleBinding, GroupScheduleViewModel>() {
    override val viewModelClass: Class<GroupScheduleViewModel> = GroupScheduleViewModel::class.java
    override val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> FragmentGroupScheduleBinding =
        { inflater, parent, attach -> FragmentGroupScheduleBinding.inflate(inflater, parent, attach) }

    override fun onInit() {
        super.onInit()
        if (!Updates.groupSchedule) viewModel.refreshSchedule()
        if (UserSettings.section == Section.GROUP) hideTeacherMenu()
        binding.groupName.text = UserSettings.group.name
        setOnRefreshListener(binding.refreshSchedule)
        initMondayView()
        initTuesdayView()
        initWednesdayView()
        initThursdayView()
        initFridayView()
        initSaturdayView()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        with(binding) {
            setting.setOnClickListener { navController.navigate(
                R.id.action_groupScheduleFragment_to_settingsFragment,
                bundleOf("back" to Section.GROUP)) }
            posts.setOnClickListener { navController.navigate(
                R.id.action_groupScheduleFragment_to_postsFragment) }
            teacherMenu.setOnClickListener { navController.navigate(
                R.id.action_groupScheduleFragment_to_teacherScheduleFragment) }
            aboutButton.setOnClickListener { navController.navigate(
                R.id.action_groupScheduleFragment_to_aboutFragment) }
            expand.setOnClickListener { expand() }
            collapse.setOnClickListener { collapse() }
            monday.setOnClickListener { activateDay(Days.MONDAY) }
            tuesday.setOnClickListener { activateDay(Days.TUESDAY) }
            wednesday.setOnClickListener { activateDay(Days.WEDNESDAY) }
            thursday.setOnClickListener { activateDay(Days.THURSDAY) }
            friday.setOnClickListener { activateDay(Days.FRIDAY) }
            saturday.setOnClickListener { activateDay(Days.SATURDAY) }
        }
    }

    override fun onViewLifecycle(owner: LifecycleOwner) {
        super.onViewLifecycle(owner)
        viewModel.refreshInLive.observe(owner) {
            binding.refreshSchedule.isRefreshing = false
        }
        viewModel.errorLive.observe(owner) {
            binding.refreshSchedule.isRefreshing = false
            if (it.exception is InternalException.OperationException) {
                showMessage(it.exception.message)
            } else showMessage(getString(R.string.unknown_exception))
        }
    }

    private fun hideTeacherMenu() {
        binding.teacherMenu.visibility = View.INVISIBLE
        val params = binding.group.layoutParams as ConstraintLayout.LayoutParams
        params.horizontalBias = 0.5f
        binding.group.layoutParams = params
    }

    override fun expand() {
        super.expand()
        binding.expand.visibility = View.INVISIBLE
        binding.collapse.visibility = View.VISIBLE
        binding.menu.visibility = View.INVISIBLE
    }

    override fun collapse() {
        super.collapse()
        binding.expand.visibility = View.VISIBLE
        binding.collapse.visibility = View.INVISIBLE
        binding.menu.visibility = View.VISIBLE
    }

    private fun initMondayView() {
        mondayAdapter = ScheduleAdapter(mondaySchedule, true)
        binding.mondayView.layoutManager = LinearLayoutManager(context)
        binding.mondayView.adapter = mondayAdapter
    }

    private fun initTuesdayView() {
        tuesdayAdapter = ScheduleAdapter(tuesdaySchedule, true)
        binding.tuesdayView.layoutManager = LinearLayoutManager(context)
        binding.tuesdayView.adapter = tuesdayAdapter
    }

    private fun initWednesdayView() {
        wednesdayAdapter = ScheduleAdapter(wednesdaySchedule, true)
        binding.wednesdayView.layoutManager = LinearLayoutManager(context)
        binding.wednesdayView.adapter = wednesdayAdapter
    }

    private fun initThursdayView() {
        thursdayAdapter = ScheduleAdapter(thursdaySchedule, true)
        binding.thursdayView.layoutManager = LinearLayoutManager(context)
        binding.thursdayView.adapter = thursdayAdapter
    }

    private fun initFridayView() {
        fridayAdapter = ScheduleAdapter(fridaySchedule, true)
        binding.fridayView.layoutManager = LinearLayoutManager(context)
        binding.fridayView.adapter = fridayAdapter
    }

    private fun initSaturdayView() {
        saturdayAdapter = ScheduleAdapter(saturdaySchedule, true)
        binding.saturdayView.layoutManager = LinearLayoutManager(context)
        binding.saturdayView.adapter = saturdayAdapter
    }
}