package com.bip.schedule_app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.bip.schedule_app.dialog.showStandardMessage
import com.bip.schedule_app.value.Theme
import com.bip.schedule_app.value.UserSettings

abstract class BaseFragment<VB: ViewBinding>: Fragment() {
    abstract val onBinding: (LayoutInflater, ViewGroup?, Boolean) -> VB
    protected lateinit var navController: NavController
    protected val binding get() = _binding!!
    private var _binding: VB? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = onBinding(inflater, container, false)
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = provideNavController()
        onInit()
        onClickListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewLifecycleOwnerLiveData.observe(this, {
            if (it == null) return@observe
            requireActivity().onBackPressedDispatcher.addCallback(it, backPressedCallback)
            onViewLifecycle(it)
        })
    }

    protected open var customBackNavigation: Boolean = false
        set(value) {
            field = value
            backPressedCallback.isEnabled = value
        }

    private val backPressedCallback: OnBackPressedCallback by lazy {
        object : OnBackPressedCallback(customBackNavigation) {
            override fun handleOnBackPressed() {
                onCustomBackNavigation()
            }
        }
    }

    fun setTheme(theme: Theme) {
        AppCompatDelegate.setDefaultNightMode(theme.system)
        UserSettings.theme = theme
    }

    open fun onCustomBackNavigation() {}

    open fun provideNavController(): NavController {
        return findNavController()
    }

    open fun onInit() { }

    open fun onClickListeners() { }

    open fun onViewLifecycle(owner: LifecycleOwner) {}

    open fun showMessage(message: String) {
        requireActivity().showStandardMessage(message)
    }
}