package com.bip.schedule_app.repos

import com.bip.schedule_app.db.ScheduleDB
import com.bip.schedule_app.db.ScheduleType
import com.bip.schedule_app.db.dao.ScheduleDao
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.model.*
import com.bip.schedule_app.retrofit.RemoteData
import com.bip.schedule_app.value.Days
import com.bip.schedule_app.value.Updates
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TeacherScheduleRepo @Inject constructor(
    private val scheduleDao: ScheduleDao,
    private val remoteData: RemoteData,
    private val timeRepo: TimeRangeRepo
) {
    private val type = ScheduleType.TEACHER

    val mondaySchedule = scheduleDao.getSchedule(type.name, Days.MONDAY.dayNumber)
    val tuesdaySchedule = scheduleDao.getSchedule(type.name, Days.TUESDAY.dayNumber)
    val wednesdaySchedule = scheduleDao.getSchedule(type.name, Days.WEDNESDAY.dayNumber)
    val thursdaySchedule = scheduleDao.getSchedule(type.name, Days.THURSDAY.dayNumber)
    val fridaySchedule = scheduleDao.getSchedule(type.name, Days.FRIDAY.dayNumber)
    val saturdaySchedule = scheduleDao.getSchedule(type.name, Days.SATURDAY.dayNumber)

    suspend fun refreshTeacherSchedule() {
        if (!Updates.time) timeRepo.refreshTimeRanges()
        val response = remoteData.getTeacherSchedule()

        if (response.isSuccessful) {
            scheduleDao.delete(type.name)
            scheduleDao.save(scheduleToDays(response.body()))
            Updates.teacherSchedule = true
        } else throw InternalException.OperationException(response.message())
    }

    private fun emptySchedule(time: Time, day: Int): ScheduleDB {
        return ScheduleDB(id = 0,
            number = time.lessonNumber,
            group = Group(0, ""),
            weekday = Weekday(day, ""),
            lesson = Lesson(0, ""),
            teacher = Teacher(0, ""),
            room = Room(0, ""),
            time = time,
            type = type.name)
    }

    private fun scheduleToDays(schedules: MutableList<Schedule>?): MutableList<ScheduleDB> {
        val fullList: MutableList<ScheduleDB> = ArrayList()
        if (schedules == null) return fullList
        for (day in Days.values()) {
            fullList.addAll(when (day) {
                Days.MONDAY -> getDaySchedule(schedules, timeRepo.getDayTime(day), day.dayNumber)
                Days.TUESDAY -> getDaySchedule(schedules, timeRepo.getDayTime(day), day.dayNumber)
                Days.WEDNESDAY -> getDaySchedule(schedules, timeRepo.getDayTime(day), day.dayNumber)
                Days.THURSDAY -> getDaySchedule(schedules, timeRepo.getDayTime(day), day.dayNumber)
                Days.FRIDAY -> getDaySchedule(schedules, timeRepo.getDayTime(day), day.dayNumber)
                Days.SATURDAY -> getDaySchedule(schedules, timeRepo.getDayTime(day), day.dayNumber)
            })
        }
        return fullList
    }

    private fun getDaySchedule(schedules: MutableList<Schedule>,
                               timeRange: MutableList<Time>, day: Int): MutableList<ScheduleDB> {
        val dayList: MutableList<ScheduleDB> = ArrayList()
        for (time in timeRange) {
            val schedule = schedules.find { s -> s.weekday.id == day && s.number == time.lessonNumber }
            if (schedule == null) dayList.add(emptySchedule(time, day))
            else dayList.add(schedule.getScheduleDB(type))
        }
        return dayList
    }
}