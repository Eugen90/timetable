package com.bip.schedule_app.repos

import com.bip.schedule_app.db.dao.TimeDao
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.retrofit.RemoteData
import com.bip.schedule_app.value.Days
import com.bip.schedule_app.value.Updates
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TimeRangeRepo @Inject constructor(
    private val timeDao: TimeDao,
    private val remoteData: RemoteData) {

    fun getDayTime(day: Days) = timeDao.getDayTimeRange(day.dayNumber)

    suspend fun refreshTimeRanges() {
        val response = remoteData.getTimeRange()
        if (response.isSuccessful) {
            timeDao.delete()
            response.body()?.let { timeDao.save(it) }
            Updates.time = true
        } else throw InternalException.OperationException(response.message())
    }
}