package com.bip.schedule_app.repos

import com.bip.schedule_app.db.dao.PostsListDao
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.retrofit.RemoteData
import com.bip.schedule_app.value.Updates
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostsRepo @Inject constructor(
    private val postsDao: PostsListDao,
    private val remoteData: RemoteData
) {

    val postsList = postsDao.getPosts()

    suspend fun refreshPosts() {
        val response = remoteData.getPosts()
        if (response.isSuccessful) {
            if (response.body()?.isNotEmpty() == true) {
                postsDao.delete()
            }
            response.body()?.let { postsDao.save(it) }
            Updates.post = true
        } else throw InternalException.OperationException(response.message())
    }
}