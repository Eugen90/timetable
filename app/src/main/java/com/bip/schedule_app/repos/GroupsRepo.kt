package com.bip.schedule_app.repos

import com.bip.schedule_app.db.dao.GroupListDao
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.retrofit.RemoteData
import com.bip.schedule_app.value.Updates
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GroupsRepo @Inject constructor(
    private val groupDao: GroupListDao,
    private val remoteData: RemoteData
) {
    val groupList = groupDao.getGroups()

    suspend fun refreshGroupList() {
        val response = remoteData.getGroups()
        if (response.isSuccessful) {
            if (response.body()?.isNotEmpty() == true) {
                groupDao.delete()
            }
            response.body()?.let { groupDao.save(it) }
            Updates.group = true
        } else throw InternalException.OperationException(response.message())
    }
}