package com.bip.schedule_app.repos

import com.bip.schedule_app.db.dao.TeacherListDao
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.retrofit.RemoteData
import com.bip.schedule_app.value.Updates
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TeachersRepo @Inject constructor(
    private val teachersDao: TeacherListDao,
    private val remoteData: RemoteData
) {
    val teacherList = teachersDao.getTeachers()

    suspend fun refreshTeacherList() {
        val response = remoteData.getTeachers()
        if (response.isSuccessful) {
            if (response.body()?.isNotEmpty() == true) {
                teachersDao.delete()
            }
            response.body()?.let { teachersDao.save(it) }
            Updates.teacher = true
        } else throw InternalException.OperationException(response.message())
    }
}