package com.bip.schedule_app.repos

import com.bip.schedule_app.db.ScheduleDB
import com.bip.schedule_app.db.ScheduleType
import com.bip.schedule_app.db.dao.ScheduleDao
import com.bip.schedule_app.extansion.InternalException
import com.bip.schedule_app.model.Schedule
import com.bip.schedule_app.retrofit.RemoteData
import com.bip.schedule_app.value.Days
import com.bip.schedule_app.value.Updates
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GroupScheduleRepo @Inject constructor(
    private val scheduleDao: ScheduleDao,
    private val remoteData: RemoteData
) {
    private val type = ScheduleType.GROUP

    val mondaySchedule = scheduleDao.getSchedule(type.name, Days.MONDAY.dayNumber)
    val tuesdaySchedule = scheduleDao.getSchedule(type.name, Days.TUESDAY.dayNumber)
    val wednesdaySchedule = scheduleDao.getSchedule(type.name, Days.WEDNESDAY.dayNumber)
    val thursdaySchedule = scheduleDao.getSchedule(type.name, Days.THURSDAY.dayNumber)
    val fridaySchedule = scheduleDao.getSchedule(type.name, Days.FRIDAY.dayNumber)
    val saturdaySchedule = scheduleDao.getSchedule(type.name, Days.SATURDAY.dayNumber)

    suspend fun refreshSchedule() {
        val response = remoteData.getGroupSchedule()

        if (response.isSuccessful) {
            scheduleDao.delete(type.name)
            response.body()?.let { scheduleDao.save(convert(it)) }
            Updates.groupSchedule = true
        } else throw InternalException.OperationException(response.message())
    }

    private fun convert(list: MutableList<Schedule>): MutableList<ScheduleDB> {
        val convertedList: MutableList<ScheduleDB> = ArrayList()

        for (schedule in list) {
            convertedList.add(schedule.getScheduleDB(type))
        }
        return convertedList
    }
}