package com.bip.schedule_app.retrofit

import com.bip.schedule_app.value.UserSettings
import javax.inject.Inject

class RemoteData @Inject constructor(private val service: ApiService) {
    suspend fun getTeachers() = service.getTeachers()
    suspend fun getGroups() = service.getGroups()
    suspend fun getGroupSchedule() = service.getGroupSchedule(groupId = UserSettings.group.id)
    suspend fun getTeacherSchedule() = service.getTeacherSchedule(teacherId = UserSettings.teacher.id)
    suspend fun getTimeRange() = service.getTimeRange()
    suspend fun getPosts() = service.getPosts()
}