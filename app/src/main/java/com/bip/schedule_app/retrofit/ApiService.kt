package com.bip.schedule_app.retrofit

import com.bip.schedule_app.model.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/scheduleTool.v2/teachers")
    suspend fun getTeachers(): Response<MutableList<Teacher>>

    @GET("/scheduleTool.v2/groups")
    suspend fun getGroups(): Response<MutableList<Group>>

    @GET("/scheduleTool.v2/posts")
    suspend fun getPosts(/*@Query("limit") limit: Int,
                         @Query("page") page: Int*/): Response<MutableList<Post>>

    @GET("/scheduleTool.v2/schedule")
    suspend fun getGroupSchedule(@Query("group_id") groupId: Int): Response<MutableList<Schedule>>

    @GET("/scheduleTool.v2/schedule")
    suspend fun getTeacherSchedule(@Query("teacher_id") teacherId: Int): Response<MutableList<Schedule>>

    @GET("/scheduleTool.v2/time-range")
    suspend fun getTimeRange(): Response<MutableList<Time>>
}