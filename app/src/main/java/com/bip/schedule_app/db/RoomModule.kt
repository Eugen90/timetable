package com.bip.schedule_app.db

import android.content.Context
import androidx.room.Room
import com.bip.schedule_app.db.dao.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): LocalDB {
        return Room
            .databaseBuilder(context, LocalDB::class.java, "Schedule")
            .build()
    }

    @Provides
    fun provideGroupDao(db: LocalDB): GroupListDao = db.groupDao()

    @Provides
    fun providesTeacherDao(db: LocalDB): TeacherListDao = db.teacherDao()

    @Provides
    fun providesPostsDao(db: LocalDB): PostsListDao = db.postsDao()

    @Provides
    fun providesScheduleDao(db: LocalDB): ScheduleDao = db.scheduleDao()

    @Provides
    fun providesTimeDao(db: LocalDB): TimeDao = db.timeDao()
}