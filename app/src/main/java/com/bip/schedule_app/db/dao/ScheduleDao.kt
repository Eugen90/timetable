package com.bip.schedule_app.db.dao

import androidx.room.*
import com.bip.schedule_app.db.ScheduleDB
import kotlinx.coroutines.flow.Flow

@Dao
interface ScheduleDao {
    @Query("SELECT * FROM ScheduleDB WHERE type = :type AND weekday_id = :weekday ORDER BY lessonNumber")
    fun getSchedule(type: String, weekday: Int): Flow<MutableList<ScheduleDB>>

    @Query("SELECT * FROM ScheduleDB WHERE type = :type ORDER BY lessonNumber")
    fun getSchedule(type: String): MutableList<ScheduleDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(scheduleList: MutableList<ScheduleDB>)

    @Query("DELETE FROM ScheduleDB WHERE type = :type")
    fun delete(type: String)
}