package com.bip.schedule_app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bip.schedule_app.model.Group
import kotlinx.coroutines.flow.Flow

@Dao
interface GroupListDao {
    @Query("SELECT * FROM `Group` ORDER BY group_name")
    fun getGroups(): Flow<MutableList<Group>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(groupList: MutableList<Group>)

    @Query("DELETE FROM `Group`")
    fun delete()
}