package com.bip.schedule_app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bip.schedule_app.model.Time

@Dao
interface TimeDao {
    @Query("SELECT * FROM Time WHERE dayNumber = :dayNumber ORDER BY lessonNumber")
    fun getDayTimeRange(dayNumber: Int): MutableList<Time>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(timeRange: MutableList<Time>)

    @Query("DELETE FROM Time")
    fun delete()
}