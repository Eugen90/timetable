package com.bip.schedule_app.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bip.schedule_app.db.dao.*
import com.bip.schedule_app.model.Group
import com.bip.schedule_app.model.Post
import com.bip.schedule_app.model.Teacher
import com.bip.schedule_app.model.Time

@Database(
    version = 1,
    entities = [
        Group::class,
        Teacher::class,
        Post::class,
        Time::class,
        ScheduleDB::class
    ])
abstract class LocalDB: RoomDatabase() {
    abstract fun groupDao(): GroupListDao
    abstract fun teacherDao(): TeacherListDao
    abstract fun postsDao(): PostsListDao
    abstract fun timeDao(): TimeDao
    abstract fun scheduleDao(): ScheduleDao
}