package com.bip.schedule_app.db

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bip.schedule_app.model.*

@Entity
data class ScheduleDB(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var number: Int,
    @Embedded
    var group: Group,
    @Embedded
    var weekday: Weekday,
    @Embedded
    var lesson: Lesson,
    @Embedded
    var teacher: Teacher,
    @Embedded
    var room: Room,
    @Embedded
    var time: Time,
    var type: String
)
