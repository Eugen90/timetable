package com.bip.schedule_app.db.dao

import androidx.room.*
import com.bip.schedule_app.model.Post
import kotlinx.coroutines.flow.Flow

@Dao
interface PostsListDao {
    @Query("SELECT * FROM Post")
    fun getPosts(): Flow<MutableList<Post>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(postList: MutableList<Post>)

    @Query("DELETE FROM Post")
    fun delete()
}