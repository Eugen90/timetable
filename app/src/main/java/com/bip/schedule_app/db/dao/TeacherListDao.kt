package com.bip.schedule_app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bip.schedule_app.model.Teacher
import kotlinx.coroutines.flow.Flow

@Dao
interface TeacherListDao {
    @Query("SELECT * FROM Teacher ORDER BY teacher_name")
    fun getTeachers(): Flow<MutableList<Teacher>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(teachers: MutableList<Teacher>)

    @Query("DELETE FROM Teacher")
    fun delete()
}