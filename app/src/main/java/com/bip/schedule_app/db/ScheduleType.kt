package com.bip.schedule_app.db

enum class ScheduleType {
    TEACHER, GROUP
}